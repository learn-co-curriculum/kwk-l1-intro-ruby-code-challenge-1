## Today's Code Challenge!

**1.** Create a file called `freak_out.rb`. You can create this in terminal by entering `touch freak_out.rb`

**2.** Your program could ask for a user’s name and age, and returns a string that says, “OMG! NO WAY. GET OUT OF TOWN. Are you `user_name? You’re user_age years old. I’m your_age years old! That means we’re only difference_in_ages` years apart!!!”

<p data-visibility='hidden'>KWK-L1 Today's Code Challenge!</p>
